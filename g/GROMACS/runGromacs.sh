#!/bin/bash 
#SBATCH --cpus-per-task=1
#SBATCH --ntasks=20
#SBATCH --time=0:15:0
#SBATCH --partition=shared-gpu-EL7
#SBATCH --gpus=2 #you may choose the gpu type like: gpus=pascal:2 

ml fosscuda/2019b GROMACS/2020

# Warning: GROMACS will only work on nodes V3 or later
#

# Example to test GROMACS
# http://www.mdtutorials.com/gmx/lysozyme/index.html
# wget https://files.rcsb.org/download/1AKI.pdb
# grep -v HOH 1AKI.pdb > 1AKI_clean.pdb
# gmx pdb2gmx -f 1AKI_clean.pdb -o 1AKI_processed.gro -water spce
# type 15 and enter
# gmx editconf -f 1AKI_processed.gro -o 1AKI_newbox.gro -c -d 1.0 -bt cubic
# gmx_mpi solvate -cp 1AKI_newbox.gro -cs spc216.gro -o 1AKI_solv.gro -p topol.top
# wget http://www.mdtutorials.com/gmx/lysozyme/Files/ions.mdp
# gmx grompp -f ions.mdp -c 1AKI_solv.gro -p topol.top -o ions.tpr
# gmx genion -s ions.tpr -o 1AKI_solv_ions.gro -p topol.top -pname NA -nname CL -neutral
# choose 13
# wget http://www.mdtutorials.com/gmx/lysozyme/Files/minim.mdp
# gmx grompp -f minim.mdp -c 1AKI_solv_ions.gro -p topol.top -o em.tpr

# this will be launched on a node with gpu.

srun gmx_mpi mdrun -v -deffnm em
