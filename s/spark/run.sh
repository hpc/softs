#!/bin/bash
# UNIGE HPC
# based on https://researchcomputing.princeton.edu/support/knowledge-base/spark
# and https://gitlab.mpcdf.mpg.de/jkennedy/Spark-on-Slurm

#SBATCH --job-name=spar          # create a short name for your job
#SBATCH --nodes=2                # node count
#SBATCH --ntasks-per-node=4      # number of tasks per node
#SBATCH --cpus-per-task=2        # cpu-cores per task (>1 if multi-threaded tasks)
#SBATCH --mem=20G                # memory per node
#SBATCH --time=00:05:00          # total run time limit (HH:MM:SS)


TEC=$(($SLURM_NNODES * $SLURM_NTASKS_PER_NODE * $SLURM_CPUS_PER_TASK))
EM=$(($SLURM_MEM_PER_NODE / $SLURM_NTASKS_PER_NODE / 1024))

export SPARK_LOG_DIR=logs
export SPARK_WORKER_DIR=work

ml fosscuda/2020b Spark/3.1.1

# This Spark module is using Java 11 and this seems to produce warnings. We force to use Java 8.
ml Java/1.8

# This is a helper to start the master and workers
./spark-start

echo $MASTER | tee master.txt
echo $TEC | tee -a master.txt
echo $EM | tee -a master.txt

spark-submit --total-executor-cores ${TEC} --executor-memory ${EM}G pi.py 1000
