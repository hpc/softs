#!/bin/sh
## purpose: test the allocation of multiple steps in parallel
## without using --exact the behaviour isn't as expected. According to the doc:
##
## -- exact Allow a step access to only the resources requested for the step. By default, all non-GRES
## resources on each node in the step allocation will be used. This option only applies to step allocations.


#SBATCH --partition=debug-cpu
#SBATCH --ntasks=2
#SBATCH --cpus-per-task=1

echo Number of tasks: $SLURM_NTASKS

for i in $(seq 1 $SLURM_NTASKS); do

  srun -n1 -c1 --exact ./job.sh $(hostname) $i &

done

wait
