#!/bin/sh


## vars
SRUN=''
SRUN_OPTIONS=''
SECONDS=100000
PAUSE_DELAY=600000


## Slurm
#SBATCH --job-name=SAT
#SBATCH --exclusive


## checks
SAT_CMD=/opt/cluster/src/dalco/stressapptest-1.0.6_autoconf/src/stressapptest
if [ ! -x "${SAT_CMD}" ]; then
    echo "${SAT_CMD} not executable."
    exit 1
fi

if [ "x$1" = x-h ]; then
    cat <<EOF
Usage:
  - standalone: $0 [stressapptest options]
  - Slurm:      sbatch --nodelist=NODELIST [...] $0 [stressapptest options]
EOF
    exit 1
elif [ "x$1" = x--all-cpus ]; then
    SRUN='srun'
    SRUN_OPTIONS='--cpu_bind=mask_cpu:0xffffffff'
    shift
fi


## main
cat <<EOF
I: parameters:
   - seconds: ${SECONDS}
   - pause_delay: ${PAUSE_DELAY}
EOF

echo "I: running..."
${SRUN} ${SRUN_OPTIONS} \
 ${SAT_CMD} \
    -s ${SECONDS} \
    --pause_delay ${PAUSE_DELAY} \
    "$@"
