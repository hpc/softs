#!/bin/bash
#SBATCH --job-name=testMPI4py
#SBATCH --ntasks-per-node=2
#SBATCH --time=00:02:00
#SBATCH --partition=debug-cpu

ml foss/2020b mpi4py/3.0.3-timed-pingpong
#ml foss/2020b Python/3.8.6

srun python point-to-point.py
