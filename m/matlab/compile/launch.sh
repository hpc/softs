#!/bin/bash
#SBATCH --partition=debug-cpu
#SBATCH --ntasks=1

# launch a matlab binary (compiled .m)
module load foss/2022a MATLAB/2022a

export LD_PRELOAD=${EBROOTMATLAB}/bin/glnxa64/glibc-2.17_shim.so

NAME=hpcmaster

srun ./hello $NAME 
