#!/bin/sh
#SBATCH --tasks 1
#SBATCH --cpus-per-task 8
#SBATCH --partition debug-cpu
#SBATCH --licenses=matlab@matlablm.unige.ch
#SBATCH --licenses=distrib_computing_toolbox@matlablm.unige.ch
#SBATCH --time 05:00

# in this sbatch file, we request eight core for a matlab job

module load MATLAB/2022a

srun matlab -nodesktop -nosplash -nodisplay -r testParFor
