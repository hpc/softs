#!/bin/sh

#SBATCH --partition=shared-gpu-EL7
#SBATCH --gres=gpu:1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --time=10:00

module load GCCcore/8.2.0 Singularity/3.4.0-Go-1.12

OPENPOSE_SIMG=openpose.simg
VIDEO_INPUT=examples/media/video.avi
VIDEO_OUTPUT=$HOME/tests/output.avi

srun singularity exec --nv --pwd /openpose-master $OPENPOSE_SIMG build/examples/openpose/openpose.bin --video $VIDEO_INPUT --write_video $VIDEO_OUTPUT --no_display
