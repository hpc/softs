#!/bin/sh
#
# <https://gitlab.unige.ch/prods/ies/recherche/hpc/-/issues/1388>
#  <https://doc.eresearch.unige.ch/hpc-admin/administration_maintenance_and_procedures?rev=1598954419#check_after_maintenance>


#SBATCH --time=00:10:00


## vars
EXIT_CODE=0


echo "I: full hostname: $(hostname -f)"
echo "I: Slurm version: $(rpm -q slurm | \
                           sed -e 's/^slurm-\(.*\)\.el.*$/\1/')"
echo

## main
for I in $(module spider foss 2>&1 | \
            awk '/^[ ]*foss\/20/ {print $1}'); do
    ### load foss
    module purge
    module load "${I}"

    ### tests
    echo "I: checking OpenMPI/${EBVERSIONOPENMPI} (foss/${EBVERSIONFOSS})..."

    #### <https://gitlab.unige.ch/hpc/doc/-/commit/26f5a7026d9dda19e61dd0eb784043924bdf3be7>
    ##### ATTENTION, since Slurm_18.08 Slurm's libpmi.so is
    ##### linked to libslurmfull.so and no more libslurm.so
    ##### <https://github.com/open-mpi/ompi/issues/5145#issuecomment-413517387>
    #####  <https://bugs.schedmd.com/show_bug.cgi?id=4918#c28>
    ###### ATTENTION, removing libslurm.so check since it will be
    ###### anyway catched later on while testing `mpicc`
    ###### <https://gitlab.unige.ch/prods/ies/recherche/hpc/-/issues/1388#note_66075>
    if ! ldd "${EBROOTOPENMPI}/lib/libmpi.so" | \
          grep -q -e 'libslurmfull.so'; then
        ##### ATTENTION, it is impossible to know the Slurm version
        ##### which OpenMPI was compiled against to...
        ##### <https://gitlab.unige.ch/prods/ies/recherche/hpc/-/issues/1388#note_66316>
        MCA_PMIX_S1_FULLPATH="${EBROOTOPENMPI}/lib/openmpi/mca_pmix_s1.so"
        if [ -r "${MCA_PMIX_S1_FULLPATH}" ]; then
            if ! ldd "${MCA_PMIX_S1_FULLPATH}" 2>&1 | \
                  grep -q -e 'libslurmfull.so'; then
                ERROR_MSG="E: openmpi/mca_pmix_s1.so not compiled against libslurmfull.so."
                EXIT_CODE=1
                ##### ATTENTION, new library since Slurm_20.11...
                ##### <https://gitlab.unige.ch/prods/ies/recherche/hpc/-/issues/2175>
                #####  <https://gitlab.unige.ch/prods/ies/recherche/hpc/-/issues/1912>
                if ! ldd "${MCA_PMIX_S1_FULLPATH}" 2>&1 | \
                      grep -q -e 'libslurm_pmi.so'; then
                    ERROR_MSG="E: openmpi/mca_pmix_s1.so not compiled against libslurm_pmi.so."
                    EXIT_CODE=1
                else
                    ERROR_MSG=""
                    EXIT_CODE=0
                fi
                [ -n "${ERROR_MSG}" ] && \
                 echo "${ERROR_MSG}"
            fi
        else
            echo "E: libmpi.so not compiled against libslurmfull.so."
            EXIT_CODE=1
        fi
    fi


    #### <https://gitlab.unige.ch/hpc/doc/-/commit/9ac154e7ef58a29964ddf3da356a44e04d74e7a2>
    MPICC_ERROR_MESSAGE="$(mpicc 2>&1 | \
                            head -n 1)"
    if (echo "${MPICC_ERROR_MESSAGE}" | \
         grep -q -e 'error while loading shared libraries: libslurm.so'); then
        echo "E: mpicc not able to load libslurm.so."
        EXIT_CODE=1
    elif ! (echo "${MPICC_ERROR_MESSAGE}" | \
             grep -q -e 'gcc: fatal error: no input files'); then
        echo "E: mpicc not working."
        EXIT_CODE=1
    fi

    echo
done

exit "${EXIT_CODE}"
