#!/bin/sh
#SBATCH --job-name=test
#SBATCH --partition=shared
#SBATCH --output=slurm-%J.out
#SBATCH --nodes 1
#SBATCH --exclusive

# facultatif
#export OMP_NUM_THREADS=$SLURM_JOB_CPUS_PER_NODE

# obligatoire si on ne spécifie pas le nombre de cpu par tâche.
srun --cpu_bind=mask_cpu:0xffffffff hello

# on peut également faire ainsi (max 1 noeud, pas top pour de l'hybride)
#srun -c $SLURM_JOB_CPUS_PER_NODE hello
