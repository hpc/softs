#!/usr/bin/env python
#
# Validate GPUs
#   <msgid:hpc/11feab61b1bc49c7a48a734e2fdcc736@unige.ch>
#
# Thanks to Shreyasvi Natraj <Shreyasvi.Natraj@unige.ch> for the test.


import tensorflow as tf
import socket
print("host={} | gpu available? {} | tensorflow version: {}".format(socket.gethostname(), tf.test.is_gpu_available(cuda_only=True), tf.__version__))

# <https://www.tensorflow.org/api_docs/python/tf/test/is_gpu_available>
#tf.config.list_physical_devices('GPU')

import time
time.sleep(300)
