#!/bin/sh
#SBATCH --cpus-per-task=2
#SBATCH --mem=6G

# Warnning: it seems TopHat is deprecated: https://ccb.jhu.edu/software/tophat/index.shtml
# Please note that TopHat has entered a low maintenance, low support stage as it is now largely superseded by 
# HISAT2 which provides the same core functionality (i.e. spliced alignment of RNA-Seq reads), in a more 
# accurate and **much more efficient way**.

module load GCC/7.3.0-2.30  OpenMPI/3.1.1 TopHat/2.1.1

srun tophat --num-threads=2
