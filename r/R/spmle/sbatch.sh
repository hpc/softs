#!/bin/sh

# R test with GEOS, Numpy, Scipy and SPMLE

ml foss/2020a R/4.0.0 rgeos/0.5-5-R-4.0.0 SciPy-bundle/2020.03-Python-3.8.2

INFILE=test-spmle.R
OUTFILE=log-${SLURM_JOBID}.Rout
srun R CMD BATCH $INFILE $OUTFILE
