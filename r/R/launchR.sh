#!/bin/bash
#SBATCH --job-name=testR
#SBATCH --cpus-per-task=1
#SBATCH --time=0:15:0
#SBATCH --partition=debug-cpu

module load GCC/11.3.0 OpenMPI/4.1.4 R/4.2.1

INFILE=hello.R
OUTFILE=hello.Rout

srun R CMD BATCH $INFILE $OUTFILE

