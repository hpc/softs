#!/usr/bin/env python
#
# Validate GPUs
#   <msgid:hpc/4127E2A9-9CF7-4896-83A3-4CA26DDA34CA@etu.unige.ch>
#
# Thanks to Jason Ramapuram <Jason.Ramapuram@etu.unige.ch> for the test.


import torch
import socket
print("host={} | gpu available? {} | torch version: {}".format(socket.gethostname(), torch.cuda.is_available(), torch.__version__))

import time
time.sleep(300)
