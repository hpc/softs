#!/bin/sh
#
#SBATCH --partition=shared-gpu-EL7
#SBATCH --time=00:15:00
#SBATCH --gres=gpu:1

ml fosscuda/2019b PyTorch/1.4.0-Python-3.7.4

# run a simple example found here https://pytorch.org/tutorials/beginner/pytorch_with_examples.html#autograd
# the purpose is only to be sure pytorch is running on gpu. If you have another idea, feel free to comment
# on hpc-communit.unige.ch

srun python autograd.py
