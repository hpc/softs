#!/bin/sh

#SBATCH --partition=debug-cpu
#SBATCH --cpus-per-task=2


ml GCCcore/11.3.0 Python/3.10.4

srun python multi.py
