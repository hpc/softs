"""
@author: Yann Sagon <yann.sagon@unige.ch>

Python example script that shows the difference between available CPUs through SLURM and physical CPUs on the machine
"""
import multiprocessing
import psutil

print("multiprocessing (phyiscal CPUs): ", multiprocessing.cpu_count())
print("psutil (available through cgroup/SLURM): ", len(psutil.Process().cpu_affinity()))
