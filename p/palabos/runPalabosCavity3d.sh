#!/bin/sh
#SBATCH --job-name=test-palabos
#SBATCH --cpus-per-task=1
#SBATCH --tasks=16
#SBATCH --partition=debug-cpu
#SBATCH --time=6:00



TOOLCHAIN=foss/2022a
# resolution http://wiki.palabos.org/plb_wiki:benchmarks
#N=2500
N=200
PALABOS=palabos-v2.3.0
module purge
module load $TOOLCHAIN

echo TOOLCHAIN=$TOOLCHAIN
echo PALABOS=$PALABOS
echo N=$
echo NTASKS=$SLURM_NTASKS
echo SLURM_NODELIST=$SLURM_NODELIST
echo SLURM_NNODES=$SLURM_NNODES

srun $PALABOS/examples/benchmarks/cavity3d/cavity3d $N
#srun palabos-v2.0r0/examples/benchmarks/cavity3d/cavity3d $N
