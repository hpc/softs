#!/bin/bash
#
# <https://gitlab.unige.ch/prods/ies/recherche/hpc/-/issues/1312#note_62294>
#  <https://gitlab.unige.ch/hpc/benchmark-aimp-2019/-/blob/0fe97cdf5152abe0893befc9ae1f3c8c6515212d/Palabos/README.md>


## vars
PROGNAME=${0##*/}
DIRNAME=${0%/*}
SPOOL_DIR="/opt/cluster/spool"
PALABOS_DIR="application-tests/softs/p/palabos/"
GROUP_BY_CPU_GENERATION=0
SILENT_MISSING_SLURM_OUTPUT=0
SHOW_BELOW_REFERENCE_VALUES_ONLY=0
WHOLE_CLUSTER=0


## functions
usage() {
    cat <<EOF
Usage: ${PROGNAME} [-h] <slurm_jobid> | [-s] <node> [<node>...] | [-c [-b]] [-s] -w

Options:
     -h : display this help and exit
     -b : show only jobs/nodes with values below the reference (only with -c)
     -c : group results by CPU generation (only with -w)
     -s : silent missing slurm-NNN_0.out for a node
     -w : get whole-cluster results based on \`scontrol show Nodes\'
EOF
}

print_headers() {
    cat <<EOF
I: Palabos cavity3d benchmarks results for N=200:
       NODE >    JobId   |   average  |   stdevp   |   lowest   |   highest
EOF
}

get_results_slurm_jobid() {
    ### <https://stackoverflow.com/questions/18786073/compute-average-and-standard-deviation-with-awk#18786709>
    printf "   %8s > %10d | %10f | %10f | %10f | %10f\n" \
           "${1}" \
           "${2}" \
           $(awk '/^After / { matches+=1;
                              sum+=$4;
                              sumsq+=($4)^2;
                              { if ( matches == 1 ||
                                     $4 < lowest )
                                    lowest=$4;
                                else if ( $4 > highest )
                                    highest=$4;
                              }
                            }
                  END { printf "     %f %f %f %f\n",
                               sum/matches,
                               sqrt((sumsq-(sum^2/matches))/matches),
                               lowest,
                               highest
                      }' \
                 slurm-"${2}"_[0-9]*.out)
}

get_nodes_list() {
    ### <https://stackoverflow.com/questions/19289060/awkhow-to-get-a-part-of-a-field#19290049>
    scontrol -o show Nodes | \
     awk "/AvailableFeatures=.*,${1}/ { split(\$1, subfield, \"=\");
                                        print subfield[2];
                                      }"
}

get_results_nodes() {
    for I in $@; do
        NODE_PALABOS_SPOOL_DIR="${SPOOL_DIR}/${I}/${PALABOS_DIR}"
        if ! [ -d "${NODE_PALABOS_SPOOL_DIR}" ]; then
            echo "E: ${NODE_PALABOS_SPOOL_DIR} not readable or does not exist."
            exit 1
        fi
        pushd "${NODE_PALABOS_SPOOL_DIR}" >/dev/null

        SLURM_JOBS_OUTPUT_FILES="$(ls -1 slurm-[0-9]*_0.out 2>/dev/null)"
        if [ "$(echo "${SLURM_JOBS_OUTPUT_FILES}" | \
                 grep -c -e '.out')" -eq 0 ] && \
           [ "${SILENT_MISSING_SLURM_OUTPUT}" -eq 0 ]; then
            echo "E: slurm-NNN_0.out not found in ${PWD}."
            exit 1
        fi
        for J in ${SLURM_JOBS_OUTPUT_FILES}; do
            NODE_INDEX=$(( ${NODE_INDEX} + 1))
            if ! [ -r "${J}" ]; then
                echo "E: ${J} not readable in ${PWD}."
                exit 1
            fi
            if [ "${NODE_INDEX}" -eq 1 ]; then
                print_headers
            fi
            SLURM_JOBID="$(basename "${J}" _0.out | \
                            cut -d '-' -f 2)"
            if [ "${SHOW_BELOW_REFERENCE_VALUES_ONLY}" -eq 1 ]; then
                CPU_GENERATION_REFERENCE_VALUE_NO_UNIT="$(echo "${CPU_GENERATION_RAW##*,}" | \
                                                           sed -e 's/^\([0-9]*\).*$/\1/')"
                get_results_slurm_jobid "${I}" "${SLURM_JOBID}" | \
                 awk "(\$5 < ${CPU_GENERATION_REFERENCE_VALUE_NO_UNIT}) {print \$0}"
            else
                get_results_slurm_jobid "${I}" "${SLURM_JOBID}"
            fi
        done

        popd >/dev/null
    done
}


## checks
while getopts "hbcsw" OPTION; do
    case $OPTION in
        h)
            usage
            exit 0
            ;;
        b)
            SHOW_BELOW_REFERENCE_VALUES_ONLY=1
            ;;
        c)
            GROUP_BY_CPU_GENERATION=1
            ;;
        s)
            SILENT_MISSING_SLURM_OUTPUT=1
            ;;
        w)
            WHOLE_CLUSTER=1
            ;;
        *)
            usage
            exit 1
            ;;
    esac
done
shift $((OPTIND - 1))

if [ -z "$1" ]; then
    if [ "${WHOLE_CLUSTER}" -eq 0 ]; then
        if [ "${GROUP_BY_CPU_GENERATION}" -eq 1 ]; then
            echo "E: option -c specified without option -w."
        else
            if [ "${SHOW_BELOW_REFERENCE_VALUES_ONLY}" -eq 1 ]; then
                echo "E: option -b specified without option -c."
            else
                echo "E: missing arguments."
            fi
        fi
        usage
        exit 1
    else
        if [ "${GROUP_BY_CPU_GENERATION}" -eq 0 ] && \
           [ "${SHOW_BELOW_REFERENCE_VALUES_ONLY}" -eq 1 ]; then
            echo "E: option -b specified without option -c."
            usage
            exit 1
        fi
    fi
else
    if [ "${WHOLE_CLUSTER}" -eq 1 ]; then
        echo "E: extra arguments for whole-cluster option."
        usage
        exit 1
    else
        if [ "${GROUP_BY_CPU_GENERATION}" -eq 1 ]; then
            echo "E: option -c specified without option -w."
            usage
            exit 1
        elif [ "${SHOW_BELOW_REFERENCE_VALUES_ONLY}" -eq 1 ]; then
            echo "E: option -b specified without option -c."
            usage
            exit 1
        fi
    fi
fi


## main
if [ -n "${1}" ] && \
   (echo "${1}" | \
     grep -q -e '^[0-9]*$'); then
    if [ -n "${2}" ]; then
        echo "E: extra arugments for SLURM_JOBID mode."
        usage
        exit 1
    elif ! [ -r slurm-"${1}"_0.out ]; then
        echo "E: slurm-${1}_0.out not found or not readable in ${PWD}."
        exit 1
    fi
    print_headers
    NODE="$(awk '/^I: full hostname: / {print $4}' slurm-"${1}"_0.out)"
    get_results_slurm_jobid "${NODE%%.*}" "${1}"
else
    NODE_INDEX=0
    if [ "${WHOLE_CLUSTER}" -eq 1 ]; then
        if [ "${GROUP_BY_CPU_GENERATION}" -eq 1 ]; then
            CPU_GENERATIONS_LIST_RAW="$(awk '/V[0-9]*,[0-9]*Mega / {print $1}' \
                                            "${DIRNAME}/foss_2022a_-_cavity3d_-_Palabos_2.3.0.sbatch")"
            for CPU_GENERATION_RAW in ${CPU_GENERATIONS_LIST_RAW}; do
                echo "===== CPU generation: ${CPU_GENERATION_RAW%%,*} (reference value: ${CPU_GENERATION_RAW##*,}) ====="
                get_results_nodes "$(get_nodes_list "${CPU_GENERATION_RAW%%,*}")"
                NODE_INDEX=0
                echo
            done
        else
            get_results_nodes "$(get_nodes_list '')"
        fi
    else
        get_results_nodes "$@"
    fi
fi
