#!/bin/bash

#SBATCH --partition=mono-shared
#SBATCH --time=12:00:00
#SBATCH --cpus-per-task=12
#SBATCH --output=slurm-%J.out

module load FreeSurfer/6.0.1-centos6_x86_64

srun recon-all -subject xxx -i xxx -autorecon-all -qcache -notal-check -parallel -openmp 12
