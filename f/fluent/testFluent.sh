#!/bin/sh

#SBATCH --job-name="test fluent"
#SBATCH --ntasks=32
#SBATCH --time=15:00
#SBATCH --partition=debug-cpu
#SBATCH --output=slurm-%J.out

module load ANSYS/2021R1

# Generate a list of nodes from the SLURM environment
# https://www.nsc.liu.se/~kent/python-hostlist/
python python-hostlist-1.20/hostlist --expand --append=':' --append-slurm-tasks=$SLURM_TASKS_PER_NODE \
     $SLURM_JOB_NODELIST > machinefile

MYINPUTFILE=./execution-script


fluent 3d -mpi=openmpi -pib -g -cnf=machinefile -i $MYINPUTFILE
