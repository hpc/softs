#!/bin/sh
#---------------------------------------------------------------
#         UNIX/LINUX SHELL SCRIPT FOR FALL3D PARALLEL VERSION
# --------------------------------------------------------------
#
#SBATCH --cpus-per-task=1
#SBATCH --job-name=Test Fall3d
#SBATCH --ntasks=4
#SBATCH --time=15:00
#SBATCH --partition=debug


# user modifiable parameters

RUNDIR=/home/sagon/softs.git/f/fall3d/Runs

PROBLEMNAME=Test

EXEPROG=Fall3d_par
VERSION=7.2

# do not touch below

module load foss/2016b fall3d/${VERSION}

#
PROBLEMDIR=$RUNDIR/$PROBLEMNAME       # Problem directory

if [ ! -d $PROBLEMDIR ] ; then
  echo Cannot find PROBLEMDIR=$PROBLEMDIR
  exit
fi

#
#
#  1. Paths of Fall3d input files
#
FILEINP=$PROBLEMDIR/$PROBLEMNAME.inp     # input file
FILESRC=$PROBLEMDIR/$PROBLEMNAME.src     # input file (source)
FILEGRN=$PROBLEMDIR/$PROBLEMNAME.grn     # input file (granulometry)
FILEDBS=$PROBLEMDIR/$PROBLEMNAME.dbs.nc  # input file (Database, binary)
#
# Check existence of input files
if [ ! -f $FILEINP ] ; then
  echo Cannot find FILEINP=$FILEINP
  exit
fi
#
if [ ! -f $FILESRC ] ; then
  echo Cannot find FILESRC=$FILESRC
  exit
fi
#
if [ ! -f $FILEGRN ] ; then
  echo Cannot find FILEGRN=$FILEGRN
  exit
fi
#
if [ ! -f $FILEDBS ] ; then
  echo Cannot find FILEDBS=$FILEDBS
  exit
fi
#
#  2. Paths of Fall3d output files
#
FILELOG=$PROBLEMDIR/$PROBLEMNAME.$EXEPROG-$VERSION.log # output file (log info file)
FILERES=$PROBLEMDIR/$PROBLEMNAME.res                  # output files (netCDF & MP binary)
FILEPTS=$PROBLEMDIR/$PROBLEMNAME.pts                  # output files (track points)

#
#  3. Launches the executable
#
srun $EXEPROG $FILEINP $FILESRC $FILEGRN $FILEDBS $FILELOG $FILERES $FILEPTS
