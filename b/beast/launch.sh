#!/bin/sh

#SBATCH --partition=debug-EL7
#SBATCH --cpus-per-task=2

module load GCC/8.2.0-2.31.1 Beast/2.5.2


# According to my measure, it's a waste of resources to use more than 2 cores.
#
# | nb cores | nb instances | nb threads | minutes / MSamples | ksample /s |
# | ---------|------------- | ---------- | ------------------ | ---------- |
# | 16       | 4            | 4          | 1m55s              | 8.69       |
# | 8        | 2            | 4          | 1m26s              | 11.63      |
# | 8        | 1            | 8          | 1m16s              | 13.16      |
# | 4        | 1            | 4          | 1m13s              | 13.69      |
# | 2        | 1            | 2          | 1m28s              | 11.36      |
# | 1        | 1            | 1          | 1m41s              | 9.9        |

srun beast -beagle_SSE -instances 1 -threads -1 -overwrite Primates.xml

