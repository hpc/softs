#!/bin/bash
#SBATCH --output=%J-test_adf.log
#SBATCH --error=%J-test_adf.err
#SBATCH --job-name=testadf
#SBATCH --cpus-per-task=1
#SBATCH --ntasks=32
#SBATCH --time=0:15:0
#SBATCH --partition=debug-EL7

module load ADF/2019.104

export SCRATCH_DIR=/scratch/${SLURM_JOB_NAME}.${SLURM_JOB_ID}
export SIMULATION_DIR=${PWD}/${SLURM_JOB_NAME}.${SLURM_JOB_ID}
mkdir -p ${SIMULATION_DIR}

srun mkdir -p ${SCRATCH_DIR}

export SCM_TMPDIR=${SCRATCH_DIR}
export SCM_IOBUFFERSIZE=512

PREFIX=test_adf

cd ${SIMULATION_DIR}
adf < $SLURM_SUBMIT_DIR/$PREFIX.com
