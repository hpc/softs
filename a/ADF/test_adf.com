title H2, electronic excitation calculations

atoms
H         0.000000    0.000000    0.000000
H         0.000000    0.000000    1.000000
end

xc
  model saop
end 

basis
  Type TZ2P
  Core None
end

relativistic scalar zora

scf 
  converge 1.0e-7
end

!excitations
!  lowest 2
!end

