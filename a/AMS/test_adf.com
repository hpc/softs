Symmetry
  SymmetrizeTolerance 0.001
End

System
  Atoms
              H       0.0000000000       0.0000000000       0.0000000000
              H       0.0000000000       0.0000000000       1.0000000000
  End
  Symmetrize Yes
End

task SinglePoint

Engine adf
  Relativity
    Formalism ZORA
    Level scalar
  End
  basis
     Type TZ2P
     Core None
  End
  scf
    converge 1.0e-7
  End
  title H2, electronic excitation calculations
  xc
    model saop
  End
EndEngine
