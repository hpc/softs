#!/bin/bash
#SBATCH --output=%J-test_adf.log
#SBATCH --error=%J-test_adf.err
#SBATCH --job-name=testadf
#SBATCH --cpus-per-task=1
#SBATCH --ntasks=32
#SBATCH --time=0:15:0
#SBATCH --partition=shared-cpu

module load intel-compilers/2022.2.1  impi/2021.7.1 AMS/2023.105-intelmpi

export SCRATCH_DIR=/scratch/${SLURM_JOB_NAME}.${SLURM_JOB_ID}
export SIMULATION_DIR=${PWD}/${SLURM_JOB_NAME}.${SLURM_JOB_ID}
mkdir -p ${SIMULATION_DIR}

srun mkdir -p ${SCRATCH_DIR}

export SCM_TMPDIR=${SCRATCH_DIR}
export SCM_IOBUFFERSIZE=512

export I_MPI_PMI_LIBRARY=/usr/lib64/libpmi.so

PREFIX=test_adf

cd ${SIMULATION_DIR}
ams < $SLURM_SUBMIT_DIR/$PREFIX.com
