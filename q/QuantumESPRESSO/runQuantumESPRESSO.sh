#!/bin/bash
#SBATCH --partition=debug-cpu
#SBATCH --cpus-per-task=1
#SBATCH --ntasks=4
#SBATCH --time=00:15:00

#load environment
ml GCC/12.3.0  OpenMPI/4.1.5 QuantumESPRESSO/7.3.1

ESPRESSO_TMPDIR=${TMPDIR} #local scratch

srun pw.x -i test_espresso.in > test_espresso.out
