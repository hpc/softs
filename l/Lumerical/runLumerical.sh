#!/bin/sh

#SBATCH --cpus-per-task=1
#SBATCH --ntasks=2

export ANSYSLMD_LICENSE_FILE=1055@comso-serv.unige.ch

ml foss/2022b freeglut libxml2

srun /opt/ebsofts/Lumerical/v232/bin/fdtd-engine-ompi-lcl sweep_AR_coating_example.fsp
