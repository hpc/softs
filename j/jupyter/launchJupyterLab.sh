#!/bin/sh
#
# # https://doc.eresearch.unige.ch/hpc/applications_and_libraries#jupyter_notebook

#SBATCH --partition=public-interactive-cpu
#SBATCH --cpus-per-task=2
#SBATCH --ntasks=1
#SBATCH --mem=2G
#SBATCH --time=01:00:00

# load JupyterLab
. /etc/profile.d/modules.sh
ml GCCcore/11.3.0 JupyterLab/3.5.0

export XDG_RUNTIME_DIR=""

# specify here the directory containing your notebooks
JUPYTER_NOTEBOOK_DIR=data

# sepcify here the port to listen to
PORT=1234

# Choose one of the solutions below:

# if you want to access jupyternotebook through proxy socks or x2go, it should listen to the node's ip.
IP=$SLURMD_NODENAME

# if you want to access jupyternotebook through an ssh tunnel, it should listen to the localhost (safer)
#IP=localhost

srun jupyter lab --no-browser --port=$PORT --ip=$SLURMD_NODENAME --notebook-dir=$JUPYTER_NOTEBOOK_DIR 

#--app-dir=app --config=jupyter.config
